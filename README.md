# flux-usage-stats

Jupyter Python 3 notebook and scripts to analyze Flux usage from PBS job end records.  This is intended to give us better, data-supported insights into how researchers use Flux.

Please send any questions or requests to [flux-support-staff@umich.edu](flux-support-staff@umich.edu).  Pull requests are welcome.

# Usage

To use, do the following:

```
# Clone via SSH:
git clone git@bitbucket.org:umlsait/flux-usage-stats.git

# Or, clone via HTTPS:
https://${USER}@bitbucket.org/umlsait/flux-usage-stats.git
```

Open the Jupyter notebook `flux-usage-stats.ipynb`.  A minimum of 20 GB RAM is needed, and you may want 32 - 64 GB if you will be doing your own data exploration.  The notebook does not currently take advantage of multiple cores.

Note that the data file is not included in the Git repository since it contains private information about researcher jobs.  A path to a pre-generated data file which should be readable by all HPC support staff is hard-coded near the beginning of the Jupyter notebook (`/home/markmont/flux-usage-stats/job-info-clean-20170705.csv`), or you can generate the data yourself by following the instructions in the section "Regenerating the data", below.

On Flux, you can start the notebook using either
* [ARC Connect](https://connect.arc-ts.umich/edu)
* Running the following commands:
```
module load flux-utils
qsub -I -V -A support_flux -q flux -l nodes=1:ppn=16,mem=63gb,walltime=10:00:00
cd ${PBS_O_WORKDIR}
jupyter-start
```

# Regenerating the data

To (re)generate the data, run:

```
module load python-anaconda3/latest
pip install --user elasticsearch
./step1-extract
```

This will generate a file named `job-info-raw-YYYYMMDD.csv` where `YYYYMMDD` will be replaced with the current date.  This file contains all available Flux PBS end records in CSV format.  Other than saving the Flux PBS end records in CSV format, the output is identical to what is in the Elasticsearch indices.

Next, clean the data.  Replace `YYYYMMDD` below with the date that the raw data was generated.

```
./step2-clean job-info-raw-YYYYMMDD.csv
```

This will produce a file named `./step2-clean job-info-clean-YYYYMMDD.csv` which contains a cleaned version of the data suitable for processing:

* All sizes (memory, disk space) are converted to gigabytes.  For example, a value of 0.75 for `Resource_List_pmem` means 0.75 GB (768 MB).
* All time durations are converted to seconds.
* Processor features/properties are moved from the `Resource_List_nodes` and `Resource_List_tpn` fields to the `Resource_List_features` field.
* End records without resource usage information are fixed as best as possible so they can be used rather than excluded.
* A `unit` field is added.  The unit is determined by which school or college manages the resource account under which the job was run, according to a hand-maintained mapping in the file `account-unit-mapping.txt`.  Bash functions in `account-unit-mapping-functions` assist with determining the correct unit that manages a resource account.
* A `total_requested_memory` field is added.

Here is a list of the field names in the cleaned CSV file:
```
@timestamp
Exit_status
Resource_List_advres
Resource_List_cput
Resource_List_ddisk
Resource_List_feature
Resource_List_file
Resource_List_gres
Resource_List_host
Resource_List_mem
Resource_List_ncpus
Resource_List_neednodes
Resource_List_nodect
Resource_List_nodes
Resource_List_nodeset
Resource_List_pmem
Resource_List_procs
Resource_List_qos
Resource_List_reqattr
Resource_List_size
Resource_List_software
Resource_List_tpn
Resource_List_walltime
account
cluster_name
core_seconds
ctime
end
etime
event
exec_host
group
jobid
jobname
owner
processor_equivalents
processor_equivalent_seconds
qtime
queue
queue_state
resources_used_cput
resources_used_energy_used
resources_used_mem
resources_used_vmem
resources_used_walltime
server
start
total_execution_slots
total_requested_memory
type
unique_node_count
unit
user
```
